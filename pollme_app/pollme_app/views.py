from django.http import HttpResponse
from django.shortcuts import render

import datetime

def home(request):
	now=datetime.datetime.now()
	context={'now_time':now}
	return render(request,'home.html',context) 

def current_datetime(request):
	now=datetime.datetime.now()
	html="<html><body>It is now %s.</body></html>" %now
	return HttpResponse(html)