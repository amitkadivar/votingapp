from django.urls import path
from . import views
app_name='polls'
urlpatterns = [
    path('list/',views.poll_list,name='poll_list'),
    path('detail/<int:poll_id>',views.poll_details,name='poll_detail'),
    path('detail/<int:poll_id>/vote/',views.poll_vote,name='poll_vote'),
]
