from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from .models import Poll,Choice
# Create your views here.


def poll_list(request):
    polls=Poll.objects.all()
    context={'polls':polls}
    return render(request,'polls/poll_list.html',context)

def poll_details(request,poll_id):
    poll=get_object_or_404(Poll,id=poll_id)
    context={"poll":poll}

    return render(request,'polls/poll_detail.html',context)

def poll_vote(request,poll_id):
    choice=Choice.objects.get(id=poll_id)
    choice.vote+=1
    choice.save()
    return HttpResponse("Poll Id {} ".format(poll_id))
    